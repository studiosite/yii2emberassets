<?php

namespace studiosite\yii2emberassets;

use Yii;
use yii\web\AssetBundle;

/**
 * Асетс библиотеки EmberJS Data
 *
 * @link https://github.com/components/ember-data
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property string $baseUrl
 * @property string $sourcePath
 * @property array $css
 * @property array $js
 * @property array $depends
 */
class EmberDataAsset extends AssetBundle
{
    /**
    * @var string Альяс пути где находятся асетсы
    */
    public $baseUrl = '@web';

    /**
    * @var string Альяс пути места публикации сгенерированных асетсов
    */
    public $sourcePath = '@bower/ember-data';

    /**
    * @var array Список файлов стилей по порядку подключения
    */
    public $css = [
    ];

    /**
    * @var array Список файлов JS файлов по порядку подключения
    */
    public $js = [
        'ember-data.prod.js',
    ];

    /**
    * @var array Список асетсов - зависимости текущего асетса
    */
    public $depends = [
        'yii\web\JqueryAsset',
        'studiosite\yii2emberassets\EmberAsset',
    ];
}
