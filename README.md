
yii2emberassets
=================

Асетс бадлы эмбера, выкачивает bower

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

Необходимо добавить

```
"studiosite/yii2emberassets": "*"
```

## Использование

В асетсе проэкта добавить зависимость

```php
...
public $depends = [
    ...
    'studiosite\yii2emberassets\HandlebarsAsset',
    'studiosite\yii2emberassets\EmberAsset',
    'studiosite\yii2emberassets\EmberDataAsset',
    'studiosite\yii2emberassets\EmberLocalStorageAdapterAsset',
    'studiosite\yii2emberassets\LodashAsset',
    'studiosite\yii2emberassets\EventEmitterAsset',
    ...
];
...
```