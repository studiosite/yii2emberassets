<?php

namespace studiosite\yii2emberassets;

use Yii;
use yii\web\AssetBundle;

/**
 * Асетс библиотеки ember-localstorage-adapter
 *
 * @link https://github.com/locks/ember-localstorage-adapter
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property string $baseUrl
 * @property string $sourcePath
 * @property array $css
 * @property array $js
 * @property array $depends
 */
class EmberLocalStorageAdapterAsset extends AssetBundle
{
    /**
    * @var string Альяс пути где находятся асетсы
    */
    public $baseUrl = '@web';

    /**
    * @var string Альяс пути места публикации сгенерированных асетсов
    */
    public $sourcePath = '@bower/ember-localstorage-adapter';

    /**
    * @var array Список файлов стилей по порядку подключения
    */
    public $css = [
    ];

    /**
    * @var array Список файлов JS файлов по порядку подключения
    */
    public $js = [
        'localstorage_adapter.js',
    ];

    /**
    * @var array Список асетсов - зависимости текущего асетса
    */
    public $depends = [
        'yii\web\JqueryAsset',
        'studiosite\yii2emberassets\EmberAsset',
        'studiosite\yii2emberassets\EmberDataAsset',
    ];
}
